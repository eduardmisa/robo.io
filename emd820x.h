#ifndef __EMD820x_H__
#define __EMD820x_H__


#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/times.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>


#define CARD_ID_MAX			1999
#define Password_MAX			8
#define IP_MAX				4
#define	TIME_OUT_MAX			50000
#define	TIME_OUT_MIN			1000
#define	WDT_TIME_OUT_MAX		10000
#define	WDT_TIME_OUT_MIN		10

#define CARD_TYPE_4IO			1
#define CARD_TYPE_8IO			2

#define INPORT				1
#define OUTPORT				0
#define PORT_MAX_820x			1

#define POINT_MAX_4IO			3
#define POINT_MAX_8IO			7
#define POINT_MAX_TTL			7

////////////// Error Code ////////////// 
#define NO_ERROR                0
#define INITIAL_SOCKET_ERROR	-1
#define IP_ADDRESS_ERROR	-2
/***** Device Error *****/
#define	UNLOCK_ERROR		-3
#define	LOCK_COUNTER_ERROR	-4
#define	SET_SECURITY_ERROR	-5
/*----------------------*/

#define	DEVICE_RW_ERROR		-100
#define	NO_CARD			-101		
#define	DUPLICATE_ID		-102

/******** User Parameter Error ********/
#define	ID_ERROR		-300
#define	PORT_ERROR		-301
#define	IN_POINT_ERROR		-302
#define	OUT_POINT_ERROR		-303
#define	PARAMETERS_ERROR	-305
#define	CHANGE_SOCKET_ERROR	-306
#define	UNLOCK_SECURITY_ERROR	-307
#define	PASSWORD_ERROR		-308
#define	REBOOT_ERROR		-309
#define TIME_OUT_ERROR		-310
#define	CREATE_SOCKET_ERROR	-311
#define CHANGEIP_ERROR		-312
#define	MASK_CHANNEL_ERROR	-313
#define	COUNTER_ENABLE_ERROR	-314
#define	COUNTER_DISABLE_ERROR	-315
#define	COUNTER_READ_ERROR	-316
#define	COUNTER_CLEAR_ERROR	-317
#define	TIME_ERROR		-318
/*-------------------------------------*/

typedef	char		i8;
typedef	unsigned char	u8;
typedef	short		i16;
typedef	unsigned short	u16;
typedef	long		i32;
typedef	unsigned long	u32;
typedef	float		f32;
typedef	double		f64;

#define EMD820xStatus		i32
/*************************************************************************************/
//--- Initialization & close 
EMD820xStatus EMD820x_initial(u32 CardID,u8 IP_Address[4],u16 Host_Port,u16 Remote_port,u16 TimeOut_ms,u8 *CardType);
EMD820xStatus EMD820x_close(u32 CardID);
EMD820xStatus EMD820x_firmware_version_read(u32 CardID,u8 data[2]);

//--- Input / Output
EMD820xStatus EMD820x_port_polarity_set(u32 CardID,u8 port,u8 data);
EMD820xStatus EMD820x_port_polarity_read(u32 CardID,u8 port,u8 *data);
EMD820xStatus EMD820x_port_set(u32 CardID,u8 port,u8 data);
EMD820xStatus EMD820x_port_read(u32 CardID,u8 port,u8 *data);
EMD820xStatus EMD820x_point_polarity_set(u32 CardID,u8 port,u8 point,u8 state);
EMD820xStatus EMD820x_point_polarity_read(u32 CardID,u8 port,u8 point,u8 *state);
EMD820xStatus EMD820x_point_set(u32 CardID,u8 port,u8 point,u8 state);
EMD820xStatus EMD820x_point_read(u32 CardID,u8 port,u8 point,u8 *state);

//--- Counter 
EMD820xStatus EMD820x_counter_mask_set(u32 CardID,u8 port,u8 channel);
EMD820xStatus EMD820x_counter_enable(u32 CardID);
EMD820xStatus EMD820x_counter_disable(u32 CardID);
EMD820xStatus EMD820x_counter_read(u32 CardID,u8 port,u32 counter[8]);
EMD820xStatus EMD820x_counter_clear(u32 CardID,u8 port,u8 channel);

//--- Miscellaneous
EMD820xStatus EMD820x_socket_port_change(u32 CardID,u16 Remote_port);
EMD820xStatus EMD820x_IP_change(u32 CardID,u8 IP[4]);
EMD820xStatus EMD820x_reboot(u32 CardID);

//--- software key
EMD820xStatus EMD820x_security_unlock(u32 CardID,u8 password[8]);
EMD820xStatus EMD820x_security_status_read(u32 CardID,u8 *lock_status);
EMD820xStatus EMD820x_password_change(u32 CardID,u8 Oldpassword[8],u8 Password[8]);
EMD820xStatus EMD820x_password_set_default(u32 CardID);
EMD820xStatus EMD820x_write_mac(u32 CardID,u8 mac[6]);

//--- WDT
EMD820xStatus EMD820x_WDT_enable(u32 CardID);
EMD820xStatus EMD820x_WDT_disable(u32 CardID);
EMD820xStatus EMD820x_WDT_set(u32 CardID,u16 time,u8 state);
EMD820xStatus EMD820x_WDT_read(u32 CardID,u16 *time,u8 *state,u8 *enable);
/*************************************************************************************/



























#endif  /*- __EMD820x_H__ -*/
