VERSION 5.00
Begin VB.Form frmCountDown 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Count Down"
   ClientHeight    =   1530
   ClientLeft      =   8385
   ClientTop       =   6090
   ClientWidth     =   3600
   ControlBox      =   0   'False
   Icon            =   "frmCountDown.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1530
   ScaleWidth      =   3600
   Begin VB.Timer Timer1 
      Interval        =   300
      Left            =   0
      Top             =   0
   End
   Begin VB.Label Label1 
      Caption         =   "Wait the machine reboot:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   480
      TabIndex        =   0
      Top             =   600
      Width           =   2535
   End
End
Attribute VB_Name = "frmCountDown"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim time1 As Date
Private Sub Form_Load()
    time1 = Now
    time1 = DateAdd("s", 10, Now)
    'Unload frmMain
End Sub

Private Sub Timer1_Timer()
    Dim EndTime As Integer
    EndTime = DateDiff("s", Now, time1)
    Label1.Caption = "Wait the machine reboot:" + Str(EndTime)
    If EndTime <= 0 Then
        Unload Me
        Call frm_Initial.Show
    End If
End Sub
