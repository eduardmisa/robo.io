VERSION 5.00
Begin VB.Form About 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "About"
   ClientHeight    =   2745
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5760
   Icon            =   "About.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2745
   ScaleWidth      =   5760
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton OK 
      Caption         =   "OK"
      Height          =   495
      Left            =   4440
      TabIndex        =   0
      Top             =   1560
      Width           =   735
   End
   Begin VB.Label Label9 
      Height          =   255
      Left            =   1920
      TabIndex        =   9
      Top             =   360
      Width           =   1815
   End
   Begin VB.Image Image1 
      Height          =   765
      Left            =   3960
      Picture         =   "About.frx":030A
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1515
   End
   Begin VB.Label Label8 
      Caption         =   $"About.frx":5697
      Height          =   255
      Left            =   720
      TabIndex        =   8
      Top             =   1080
      Width           =   2535
   End
   Begin VB.Label Label7 
      Caption         =   "6F., No.100, Zhongxing Rd., Shi-Ji Dist., New Taipei "
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   840
      Width           =   3975
   End
   Begin VB.Label Label6 
      Caption         =   "FAX: +886-2-26476940"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1680
      Width           =   2055
   End
   Begin VB.Label Label5 
      Caption         =   "TEL: +886-2-26476936"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1440
      Width           =   3495
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "JS Automation  EMD820x Demo Version VB6. 1.2"
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   3510
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Copyright  2011, JS Automation "
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   2265
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "E-mail: control.cards@automation.com.tw"
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   2160
      Width           =   2925
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "http://www.automation.com.tw"
      Height          =   180
      Left            =   120
      TabIndex        =   1
      Top             =   1920
      Width           =   3105
   End
End
Attribute VB_Name = "About"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
  Dim Version(0 To 1) As Byte
  
  mintStatus = EMD820x_firmware_version_read(CardID, Version(0))
  If mintStatus = 0 Then
        Label9.Caption = "Firware Version      V" + Str(Version(1)) + "." + Trim(Str(Version(0)))
  End If
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
  'Label1.Caption = Label1.Caption + Str(Version)

End Sub

Private Sub OK_Click()
    Unload Me
End Sub
