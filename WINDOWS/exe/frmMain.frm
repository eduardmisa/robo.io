VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "EMD820x"
   ClientHeight    =   4770
   ClientLeft      =   8835
   ClientTop       =   4245
   ClientWidth     =   3075
   FillStyle       =   0  'Solid
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   15
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4770
   ScaleWidth      =   3075
   Begin VB.CheckBox m_lock 
      Height          =   495
      Left            =   2400
      Picture         =   "frmMain.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   120
      Width           =   615
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   0
      Top             =   4080
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   480
      Top             =   4080
   End
   Begin VB.CommandButton cmdexit 
      Caption         =   "&Quit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      TabIndex        =   18
      Top             =   4080
      Width           =   645
   End
   Begin VB.CheckBox Auto 
      Caption         =   "&AUTO"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1200
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   4080
      Width           =   765
   End
   Begin VB.Frame Fr9201 
      Caption         =   "I/O"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Left            =   240
      TabIndex        =   0
      Top             =   1200
      Width           =   2595
      Begin MSComDlg.CommonDialog CommonDialog1 
         Left            =   2160
         Top             =   0
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 07"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   7
         Left            =   1350
         TabIndex        =   16
         Top             =   2190
         Width           =   975
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 06"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   6
         Left            =   1350
         TabIndex        =   15
         Top             =   1950
         Width           =   975
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 05"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   5
         Left            =   1350
         TabIndex        =   14
         Top             =   1710
         Width           =   975
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 04"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   4
         Left            =   1350
         TabIndex        =   13
         Top             =   1470
         Width           =   975
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 03"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   3
         Left            =   1350
         TabIndex        =   12
         Top             =   1230
         Width           =   975
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 02"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   2
         Left            =   1350
         TabIndex        =   11
         Top             =   990
         Width           =   975
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 01"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   1350
         TabIndex        =   10
         Top             =   750
         Width           =   975
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   0
         Left            =   1350
         TabIndex        =   9
         Top             =   510
         Width           =   975
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 07"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   7
         Left            =   210
         TabIndex        =   8
         Top             =   2220
         Width           =   855
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 06"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   6
         Left            =   210
         TabIndex        =   7
         Top             =   1950
         Width           =   855
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 05"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   5
         Left            =   210
         TabIndex        =   6
         Top             =   1710
         Width           =   855
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 04"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   4
         Left            =   210
         TabIndex        =   5
         Top             =   1470
         Width           =   855
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 03"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   3
         Left            =   210
         TabIndex        =   4
         Top             =   1230
         Width           =   855
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 02"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   2
         Left            =   210
         TabIndex        =   3
         Top             =   990
         Width           =   855
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 01"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   1
         Left            =   210
         TabIndex        =   2
         Top             =   750
         Width           =   855
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "IN 00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   210
         TabIndex        =   1
         Top             =   510
         Width           =   855
      End
   End
   Begin VB.Label Label_wdt 
      Alignment       =   2  'Center
      Caption         =   "WDT DISABLE"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   720
      Width           =   2175
   End
   Begin VB.Label txtCardID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   19
      Top             =   240
      Width           =   2175
   End
   Begin VB.Image Lock_ico 
      Height          =   480
      Left            =   2520
      Picture         =   "frmMain.frx":0FD4
      Top             =   120
      Width           =   480
   End
   Begin VB.Menu m_config 
      Caption         =   "&Config"
      Begin VB.Menu m_polarity 
         Caption         =   "&Polarity"
      End
      Begin VB.Menu m_change_IP 
         Caption         =   "&Change IP"
      End
      Begin VB.Menu m_change_port 
         Caption         =   "&Change socket"
      End
   End
   Begin VB.Menu m_special 
      Caption         =   "&special"
      Begin VB.Menu m_counter 
         Caption         =   "&Counter"
      End
      Begin VB.Menu m_wdt 
         Caption         =   "&WDT"
      End
   End
   Begin VB.Menu m_security 
      Caption         =   "&Security"
      Begin VB.Menu m_chpassword 
         Caption         =   "&Change Password"
      End
      Begin VB.Menu m_pddefault 
         Caption         =   "&Set Password to default"
      End
      Begin VB.Menu m_reboot 
         Caption         =   "&Reboot"
      End
   End
   Begin VB.Menu m_help 
      Caption         =   "&Help"
      Begin VB.Menu m_contents 
         Caption         =   "&Contents"
      End
      Begin VB.Menu m_about 
         Caption         =   "&About"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim AUTO820xData As Byte
Dim Status As Integer
'Dim POINT_MAX As Integer

Private Sub Auto_Click()
    Timer2.Enabled = Auto.Value
End Sub

Private Sub CmdUnlock_Click()
    frmUnlock.Show 1
End Sub

Private Sub cmdexit_Click()
    Unload Me
    End
End Sub

Private Sub Form_Load()
    AUTO820xData = 0
    txtCardID.Caption = "ID " + Str(CardID) + " = " + frm_Initial.txtIP(0).Text + "." + frm_Initial.txtIP(1).Text + "." + frm_Initial.txtIP(2).Text + "." + frm_Initial.txtIP(3).Text
    'check the EMD820x is 4in/out or 8in/out
    If CardType = CARD_TYPE_4IO Then
        POINT_MAX = POINT_MAX_4IO
        Me.Caption = "EMD8204"
    Else
        POINT_MAX = POINT_MAX_8IO
        Me.Caption = "EMD8208"
    End If
    
    If CardType = CARD_TYPE_4IO Then
        'set screen to invisible inport and outport 5~8
        Dim i As Integer
        For i = POINT_MAX_4IO + 1 To POINT_MAX_8IO
            InP(i).Visible = False
            OutP(i).Visible = False
        Next i
        'reset the screen
        Fr9201.Height = Fr9201.Height - 850
        'Auto.Top = Fr9201.Height + Fr9201.Top + 200
        Auto.Top = Auto.Top - 850
        cmdexit.Top = Auto.Top
        'frmMain.Height = frmMain.Height - 1000
        frmMain.Height = Auto.Top + Auto.Height + 1000
    End If
    m_Global.error_index = 0
    'frmUnlock.Show 1
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Unload frmCounter
    Unload frm_WDT
    EMD820x_close CardID
    'End
End Sub

Private Sub m_about_Click()
    About.Show 1
End Sub

Private Sub m_change_IP_Click()
    If m_lock.Visible = True Then
        MsgBox "Please unlock first", , "Warning"
    ElseIf Lock_ico.Visible = True Then
        frmIP.Show 1
    End If
End Sub

Private Sub m_change_port_Click()
    If m_lock.Visible = True Then
        MsgBox "Please unlock first", , "Warning"
    ElseIf Lock_ico.Visible = True Then
        frmSocket.Show 1
    End If
End Sub

Private Sub m_chpassword_Click()
    Dim password As String
    If InputBox("Please input password(26476936) ...") = "26476936" Then
        frmSetPW.Show 1
    Else
        MsgBox "Password error !!!"
    End If
End Sub

Private Sub m_contents_Click()
    With frmMain.CommonDialog1
      .HelpFile = "sw820x.hlp"
      .HelpContext = 180
      .HelpCommand = cdlHelpContext
      .ShowHelp
    End With
End Sub

Private Sub m_counter_Click()
    If m_lock.Visible = True Then
        MsgBox "Please unlock first", , "Warning"
    Else
        frmCounter.Show
    End If
End Sub

Private Sub m_exit_Click()
    Unload Me
End Sub


Private Sub m_lock_Click()
    If m_lock.Visible = True Then
        If m_lock.Value = 0 Then
            frmUnlock.Show 1
        End If
        m_lock.Value = 0
    End If
End Sub

Private Sub m_pddefault_Click()
    Dim temp As Integer
    temp = MsgBox("It will set the password to default", vbYesNo + vbExclamation)
    If temp = vbYes Then
        Status = EMD820x_password_set_default(CardID)
        If Status = 0 Then
            Reboot_flag = True
            Unload Me
            frmCountDown.Show 1
        Else
            MsgBox "Error Code:" + Str(Status), vbOKOnly, "ERROR"
        End If
    End If
End Sub

Private Sub m_polarity_Click()
    If m_lock.Visible = True Then
        MsgBox "Please unlock first", , "Warning"
    ElseIf Lock_ico.Visible = True Then
        frm_io_polarity.Show 1
    End If
End Sub

Private Sub m_reboot_Click()
    If CardType = CARD_TYPE_4IO Then
        Status = MsgBox("It will reboot EMD8204", vbOKCancel, "warning")
    Else
        Status = MsgBox("It will reboot EMD8208", vbOKCancel, "warning")
    End If
    If Status = vbOK Then
        If m_lock.Visible = True Then
            MsgBox "Please unlock first", , "Warning"
        ElseIf Lock_ico.Visible = True Then
            Status = EMD820x_reboot(CardID)
            If Status = 0 Then
                Reboot_flag = True
                Call frmCountDown.Show
                Unload Me
            Else
                MsgBox "Reboot error:" + Str(Status), , "Warning"
            End If
        End If
    End If
End Sub

Private Sub m_wdt_Click()
    frm_WDT.Show
End Sub

Private Sub OutP_Click(Index As Integer)
    Dim state As Long
    state = EMD820x_point_set(CardID, 0, Index, OutP(Index).Value)
    'Call check_eror(state)
    m_Global.Check_ERROR (state)
End Sub


Private Sub Timer1_Timer()
    readin
    Read_Security_Status
End Sub

Private Sub readin()
    Dim i As Integer
    Dim InState As Integer
    Dim temp_data As Integer
    Dim Status As Long
    'read the inport status
    Status = EMD820x_port_read(CardID, Inport, temp_data)
    m_Global.Check_ERROR (Status)
    If Status = 0 Then
        For i = 0 To POINT_MAX
            InState = temp_data Mod 2
            If InState = 0 Then
                InP(i).Value = 0
            Else
                temp_data = temp_data - 1
                InP(i).Value = 1
            End If
            temp_data = temp_data / 2
        Next i
    End If
End Sub

Private Sub Read_Security_Status()
    Dim lock_status As Integer
    Status = EMD820x_security_status_read(CardID, lock_status)
    If lock_status = 0 Then
        m_lock.Visible = False
        Lock_ico.Visible = True
    End If
End Sub

Private Sub Timer2_Timer()
    'when click auto button,it will do follows
    If AUTO820xData > POINT_MAX Then
        AUTO820xData = 0
    End If
    Dim i As Integer
    For i = 0 To POINT_MAX
        If i <> AUTO820xData Then
            OutP(i).Value = 0
        End If
    Next i
    OutP(AUTO820xData).Value = 1
    AUTO820xData = AUTO820xData + 1
    
End Sub

