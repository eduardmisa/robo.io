VERSION 5.00
Begin VB.Form frm_WDT 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "WDT"
   ClientHeight    =   2865
   ClientLeft      =   2880
   ClientTop       =   4590
   ClientWidth     =   5160
   Icon            =   "frm_WDT.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2865
   ScaleWidth      =   5160
   Begin VB.CommandButton Initial_help 
      Height          =   555
      Left            =   3480
      Picture         =   "frm_WDT.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   2160
      Width           =   675
   End
   Begin VB.CommandButton cmd_apply 
      Caption         =   "Apply"
      Height          =   495
      Left            =   1680
      TabIndex        =   14
      Top             =   2160
      Width           =   975
   End
   Begin VB.TextBox txt_time 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3120
      TabIndex        =   11
      Text            =   "100"
      Top             =   720
      Width           =   735
   End
   Begin VB.CheckBox ck_enable 
      BackColor       =   &H0000FF00&
      Caption         =   "DISABLE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   1680
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   1200
      Width           =   2655
   End
   Begin VB.Frame Fr9201 
      Caption         =   "I/O"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1395
      Begin VB.CheckBox ck_port 
         Caption         =   "OUT 10"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   975
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "OUT 11"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   7
         Top             =   630
         Width           =   975
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "OUT 12"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   6
         Top             =   870
         Width           =   975
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "OUT 13"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   5
         Top             =   1110
         Width           =   975
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "OUT 14"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   4
         Top             =   1320
         Width           =   975
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "OUT 15"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   3
         Top             =   1560
         Width           =   975
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "OUT 16"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   2
         Top             =   1800
         Width           =   975
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "OUT 17"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   1
         Top             =   2040
         Width           =   975
      End
   End
   Begin VB.Label Label3 
      Caption         =   "WDT time = "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1680
      TabIndex        =   13
      Top             =   720
      Width           =   1335
   End
   Begin VB.Label Label2 
      Caption         =   "* 100 ms"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3960
      TabIndex        =   12
      Top             =   720
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "WDT time"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2520
      TabIndex        =   10
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "frm_WDT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub ck_enable_Click()
    If ck_enable.Value = 1 Then
        EMD820x_WDT_enable CardID
        ck_enable.Caption = "ENABLE"
        frmMain.Label_wdt.Caption = "WDT ENABLE"
    Else
        EMD820x_WDT_disable CardID
        ck_enable.Caption = "DISABLE"
        frmMain.Label_wdt.Caption = "WDT DISABLE"
    End If
End Sub

Private Sub Cmd_apply_Click()
    Dim state As Byte
    Dim timer As Integer
    Dim i As Integer
    
    Dim port_max As Integer
    port_max = 7
    
    If CardType = CARD_TYPE_4IO Then
        port_max = 3
    End If
    
    state = 0
    For i = 0 To port_max
        If ck_port(i).Value = 1 Then
            state = state + 2 ^ i
        End If
    Next i
    timer = Val(txt_time.Text)
    EMD820x_WDT_set CardID, timer, state
End Sub

Private Sub Form_Load()
    Dim timer_value As Integer
    Dim state As Byte
    Dim enable_flag As Byte
    
    Dim i As Integer
    Dim port_max As Integer
    port_max = 7
    
    If CardType = CARD_TYPE_4IO Then
        Fr9201.Height = 1455
        port_max = 3
        For i = 4 To 7
            ck_port(i).Visible = False
        Next i
    End If
    
    For i = 0 To port_max
        ck_port(i).Enabled = frmMain.OutP(i).Enabled
        ck_port(i).Caption = frmMain.OutP(i).Caption
    Next i
    EMD820x_WDT_read CardID, timer_value, state, enable_flag
    For i = 0 To port_max
        If state And (2 ^ i) Then
            ck_port(i).Value = 1
        Else
            ck_port(i).Value = 0
        End If
    Next i
    ck_enable.Value = enable_flag
    txt_time.Text = Trim(Str(timer))
End Sub

Private Sub Initial_help_Click()
    With frmMain.CommonDialog1
      .HelpFile = "sw820x.hlp"
      .HelpContext = 460
      .HelpCommand = cdlHelpContext
      .ShowHelp
    End With
End Sub
