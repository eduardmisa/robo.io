VERSION 5.00
Begin VB.Form frmIP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Change IP"
   ClientHeight    =   2010
   ClientLeft      =   4800
   ClientTop       =   5805
   ClientWidth     =   5220
   Icon            =   "frmIP.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2010
   ScaleWidth      =   5220
   Begin VB.CommandButton Initial_help 
      Height          =   615
      Left            =   3960
      Picture         =   "frmIP.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   1080
      Width           =   735
   End
   Begin VB.CommandButton Cmd_changeIP 
      Caption         =   "change IP"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   6
      Top             =   1200
      Width           =   1095
   End
   Begin VB.TextBox txtIP 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1028
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   3960
      MaxLength       =   3
      TabIndex        =   4
      Text            =   "100"
      Top             =   480
      Width           =   735
   End
   Begin VB.TextBox txtIP 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1028
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   3120
      MaxLength       =   3
      TabIndex        =   3
      Text            =   "0"
      Top             =   480
      Width           =   735
   End
   Begin VB.TextBox txtIP 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1028
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   2280
      MaxLength       =   3
      TabIndex        =   2
      Text            =   "168"
      Top             =   480
      Width           =   735
   End
   Begin VB.TextBox txtIP 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1028
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   1440
      MaxLength       =   3
      TabIndex        =   1
      Text            =   "192"
      Top             =   480
      Width           =   735
   End
   Begin VB.CommandButton CmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2400
      TabIndex        =   0
      Top             =   1200
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "IP"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   5
      Top             =   480
      Width           =   495
   End
End
Attribute VB_Name = "frmIP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Status As Integer
Private Sub Cmd_changeIP_Click()
    Dim i As Integer
    Dim IP(3) As Byte
    Status = MsgBox("When success change the IP,it will reboot EMD820x", vbOKCancel, "warning")
    If Val(txtIP(3).Text) = 0 Or Val(txtIP(3).Text) = 255 Then
        Status = vbCancel
        MsgBox "The last IP can't 0 or 255"
    End If
    If Status = vbOK Then
        For i = 0 To 3
            If Val(txtIP(i).Text) <= IP_MAX Then
                IP(i) = Val(txtIP(i).Text)
            End If
        Next i
                
        Status = EMD820x_IP_change(CardID, IP(0))
        If Status = 0 Then
            IpAddress(0) = IP(0)
            IpAddress(1) = IP(1)
            IpAddress(2) = IP(2)
            IpAddress(3) = IP(3)
            Reboot_flag = True
            Unload Me
            Unload frmMain
            frmCountDown.Show 1
        Else
            MsgBox "Change socket port error:" + Str(Status), vbOKOnly, "ERROR"
        End If
    End If
End Sub

Private Sub CmdCancel_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    txtIP(0).Text = IpAddress(0)
    txtIP(1).Text = IpAddress(1)
    txtIP(2).Text = IpAddress(2)
    txtIP(3).Text = IpAddress(3)
End Sub

Private Sub Initial_help_Click()

   With frmMain.CommonDialog1
      .HelpFile = "sw820x.hlp"
      .HelpContext = 330
      .HelpCommand = cdlHelpContext
      .ShowHelp
    End With
End Sub

'set the TextBox only input 0-9
Private Sub txtSocketPort_KeyPress(KeyAscii As Integer)
    If Not (Chr$(KeyAscii) Like "[0-9]" Or KeyAscii < 31) Then
        KeyAscii = 0
    End If
End Sub
