VERSION 5.00
Begin VB.Form frmSocket 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Change Socket port"
   ClientHeight    =   1965
   ClientLeft      =   4800
   ClientTop       =   5805
   ClientWidth     =   4830
   Icon            =   "frmSocket.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1965
   ScaleWidth      =   4830
   Begin VB.CommandButton Initial_help 
      Height          =   615
      Left            =   3360
      Picture         =   "frmSocket.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   960
      Width           =   735
   End
   Begin VB.CommandButton CmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2040
      TabIndex        =   3
      Top             =   1080
      Width           =   1095
   End
   Begin VB.CommandButton Cmd_changeport 
      Caption         =   "change port"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   480
      TabIndex        =   2
      Top             =   1080
      Width           =   1095
   End
   Begin VB.TextBox txtSocketPort 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2040
      MaxLength       =   5
      TabIndex        =   1
      Text            =   "6936"
      Top             =   360
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Remote port"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   480
      TabIndex        =   0
      Top             =   480
      Width           =   1215
   End
End
Attribute VB_Name = "frmSocket"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Status As Integer

Private Sub Cmd_changeport_Click()
    Dim Status As Integer
    
    Status = MsgBox("When success change the PORT,it will reboot EMD820x", vbOKCancel, "warning")
    If Status = vbOK Then
        If Val(txtSocketPort.Text) < PORT_MAX And txtSocketPort.Text <> "" Then
            Status = EMD820x_socket_port_change(CardID, txtSocketPort.Text)
            If Status = 0 Then
                Reboot_flag = True
                Remote_port = Val(txtSocketPort.Text)
                Unload Me
                Unload frmMain
                frmCountDown.Show 1
            Else
                MsgBox "Change socket port error:" + Str(Status), vbOKOnly, "ERROR"
            End If
        Else
            MsgBox "Input socket port error", vbOKOnly, "ERROR"
        End If
    End If
End Sub

Private Sub CmdCancel_Click()
    Unload Me
End Sub

Private Sub Form_Load()
     txtSocketPort.Text = Remote_port
End Sub

Private Sub Initial_help_Click()

   With frmMain.CommonDialog1
      .HelpFile = "sw820x.hlp"
      .HelpContext = 330
      .HelpCommand = cdlHelpContext
      .ShowHelp
    End With
End Sub

'set the TextBox only input 0-9
Private Sub txtSocketPort_KeyPress(KeyAscii As Integer)
    If Not (Chr$(KeyAscii) Like "[0-9]" Or KeyAscii < 31) Then
        KeyAscii = 0
    End If
End Sub
