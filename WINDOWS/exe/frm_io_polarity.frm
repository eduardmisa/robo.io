VERSION 5.00
Begin VB.Form frm_io_polarity 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "I/O Polarity"
   ClientHeight    =   5190
   ClientLeft      =   6225
   ClientTop       =   4620
   ClientWidth     =   3585
   Icon            =   "frm_io_polarity.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5190
   ScaleWidth      =   3585
   Begin VB.Frame Frame4 
      Height          =   4695
      Left            =   1920
      TabIndex        =   10
      Top             =   240
      Width           =   1455
      Begin VB.CommandButton Cmd_apply 
         Caption         =   "Apply"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   300
         TabIndex        =   19
         Top             =   3960
         Width           =   855
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "output7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   7
         Left            =   240
         TabIndex        =   18
         Top             =   3465
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "output6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   6
         Left            =   240
         TabIndex        =   17
         Top             =   3060
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "output5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   5
         Left            =   240
         TabIndex        =   16
         Top             =   2655
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "output4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   4
         Left            =   240
         TabIndex        =   15
         Top             =   2280
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "output3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   3
         Left            =   240
         TabIndex        =   14
         Top             =   1860
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "output2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   2
         Left            =   240
         TabIndex        =   13
         Top             =   1455
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "output1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   1
         Left            =   240
         TabIndex        =   12
         Top             =   1065
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "output0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   0
         Left            =   240
         TabIndex        =   11
         Top             =   660
         Width           =   1000
      End
      Begin VB.Label Label10 
         Alignment       =   2  'Center
         Caption         =   "OUT Port"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4695
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1455
      Begin VB.CommandButton Cmd_apply 
         Caption         =   "Apply"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   360
         TabIndex        =   21
         Top             =   3960
         Width           =   855
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "input0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   8
         Left            =   240
         TabIndex        =   8
         Top             =   660
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "input1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   9
         Left            =   240
         TabIndex        =   7
         Top             =   1065
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "input2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   10
         Left            =   240
         TabIndex        =   6
         Top             =   1455
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "input3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   11
         Left            =   240
         TabIndex        =   5
         Top             =   1860
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "input4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   12
         Left            =   240
         TabIndex        =   4
         Top             =   2265
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "input5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   13
         Left            =   240
         TabIndex        =   3
         Top             =   2655
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "input6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   14
         Left            =   240
         TabIndex        =   2
         Top             =   3060
         Width           =   1000
      End
      Begin VB.CheckBox ck_port 
         Caption         =   "input7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   15
         Left            =   240
         TabIndex        =   1
         Top             =   3465
         Width           =   1000
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "IN Port"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   9
         Top             =   240
         Width           =   735
      End
   End
End
Attribute VB_Name = "frm_io_polarity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Status As Integer

Private Sub ck_port_Click(Index As Integer)
    cmd_apply(Int(Index / 8)).Enabled = True
End Sub

Private Sub Cmd_apply_Click(Index As Integer)
    Dim channel As Integer
    Dim i As Integer

    channel = 0
    For i = 0 To 7
        If ck_port(i + Index * 8).Value = 1 Then
            channel = channel + 2 ^ i
        End If
    Next i
    Status = EMD820x_port_polarity_set(CardID, Index, channel)
    If Status = 0 Then
        cmd_apply(Index).Enabled = False
    Else
        MsgBox "Error code:" + Str(Status)
    End If
End Sub

Private Sub Form_Load()
    Dim i, j As Integer
    Dim TTL_polarity As Integer
    Dim Bit_select(8) As Byte
    Bit_select(0) = 1
    Bit_select(1) = 2
    Bit_select(2) = 4
    Bit_select(3) = 8
    Bit_select(4) = 16
    Bit_select(5) = 32
    Bit_select(6) = 64
    Bit_select(7) = 128
    
    If CardType = CARD_TYPE_4IO Then
        For i = 4 To 7
            ck_port(i).Visible = False
            ck_port(i + 8).Visible = False
        Next i
        cmd_apply(0).Top = ck_port(3).Top + ck_port(3).Height + 120
        cmd_apply(1).Top = cmd_apply(0).Top
        Frame4.Height = cmd_apply(0).Top + cmd_apply(0).Height + 200
        Frame1.Height = Frame4.Height
        frm_io_polarity.Height = Frame4.Top + Frame4.Height + 800
    End If
    For j = 0 To 1
        Status = EMD820x_port_polarity_read(CardID, j, TTL_polarity)
        
        If Status = 0 Then
            For i = 0 To 7
                If (TTL_polarity And Bit_select(i)) > 0 Then 'True Then
                    ck_port(i + j * 8).Value = 1
                    'frmMain.ck_port(i + j * 8).Caption = "OUT " + Trim(Str(j)) + Trim(Str(i))
                    'frmMain.ck_port(i + j * 8).Enabled = True
                Else
                    ck_port(i + j * 8).Value = 0
                    'frmMain.ck_port(i + j * 8).Caption = "In " + Trim(Str(j)) + Trim(Str(i))
                    'frmMain.ck_port(i + j * 8).Enabled = False
                End If
            Next i
        Else
            MsgBox "Error code:1000"
        End If
    Next j
    cmd_apply(0).Enabled = False
    cmd_apply(1).Enabled = False
    frmMain.Timer1.Enabled = False

End Sub

Private Sub Form_Unload(Cancel As Integer)
    'frmMain.readin
    Dim port(0 To 1) As Integer
    Dim i As Integer

    frmMain.Timer1.Enabled = True
    EMD820x_port_read CardID, OUTPORT, port(OUTPORT)
    EMD820x_port_read CardID, Inport, port(Inport)
    
    For i = 0 To 7
        If (port(Inport) And (2 ^ i)) > 0 Then
           frmMain.InP(i).Value = 1
        Else
           frmMain.InP(i).Value = 0
        End If
        
        If (port(OUTPORT) And (2 ^ i)) > 0 Then
           frmMain.OutP(i).Value = 1
        Else
           frmMain.OutP(i).Value = 0
        End If
    Next i
    
End Sub

