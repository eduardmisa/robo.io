VERSION 5.00
Begin VB.Form frmCounter 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Counter"
   ClientHeight    =   5280
   ClientLeft      =   3345
   ClientTop       =   3975
   ClientWidth     =   5145
   Icon            =   "frmCounter.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5280
   ScaleWidth      =   5145
   Begin VB.CommandButton Initial_help 
      Height          =   555
      Left            =   4200
      Picture         =   "frmCounter.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   49
      Top             =   4560
      Width           =   675
   End
   Begin VB.Frame Frame3 
      Height          =   4095
      Left            =   1680
      TabIndex        =   12
      Top             =   240
      Width           =   3255
      Begin VB.TextBox txtCounter 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   240
         TabIndex        =   44
         Text            =   "0"
         Top             =   720
         Width           =   855
      End
      Begin VB.CommandButton Cmd_read 
         Caption         =   "R"
         Height          =   300
         Index           =   0
         Left            =   1320
         TabIndex        =   43
         Top             =   705
         Width           =   375
      End
      Begin VB.CommandButton Cmd_clear 
         Caption         =   "C"
         Height          =   300
         Index           =   0
         Left            =   1800
         TabIndex        =   42
         Top             =   705
         Width           =   375
      End
      Begin VB.CheckBox Chk_reload 
         Height          =   375
         Index           =   0
         Left            =   2400
         TabIndex        =   41
         Top             =   720
         Width           =   375
      End
      Begin VB.CheckBox Chk_reload 
         Height          =   375
         Index           =   1
         Left            =   2400
         TabIndex        =   40
         Top             =   1125
         Width           =   375
      End
      Begin VB.CommandButton Cmd_clear 
         Caption         =   "C"
         Height          =   300
         Index           =   1
         Left            =   1800
         TabIndex        =   39
         Top             =   1095
         Width           =   375
      End
      Begin VB.CommandButton Cmd_read 
         Caption         =   "R"
         Height          =   300
         Index           =   1
         Left            =   1320
         TabIndex        =   38
         Top             =   1095
         Width           =   375
      End
      Begin VB.TextBox txtCounter 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   1
         Left            =   240
         TabIndex        =   37
         Text            =   "0"
         Top             =   1125
         Width           =   855
      End
      Begin VB.CheckBox Chk_reload 
         Height          =   375
         Index           =   2
         Left            =   2400
         TabIndex        =   36
         Top             =   1515
         Width           =   375
      End
      Begin VB.CommandButton Cmd_clear 
         Caption         =   "C"
         Height          =   300
         Index           =   2
         Left            =   1800
         TabIndex        =   35
         Top             =   1500
         Width           =   375
      End
      Begin VB.CommandButton Cmd_read 
         Caption         =   "R"
         Height          =   300
         Index           =   2
         Left            =   1320
         TabIndex        =   34
         Top             =   1500
         Width           =   375
      End
      Begin VB.TextBox txtCounter 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   2
         Left            =   240
         TabIndex        =   33
         Text            =   "0"
         Top             =   1515
         Width           =   855
      End
      Begin VB.CheckBox Chk_reload 
         Height          =   375
         Index           =   3
         Left            =   2400
         TabIndex        =   32
         Top             =   1920
         Width           =   375
      End
      Begin VB.CommandButton Cmd_clear 
         Caption         =   "C"
         Height          =   300
         Index           =   3
         Left            =   1800
         TabIndex        =   31
         Top             =   1905
         Width           =   375
      End
      Begin VB.CommandButton Cmd_read 
         Caption         =   "R"
         Height          =   300
         Index           =   3
         Left            =   1320
         TabIndex        =   30
         Top             =   1905
         Width           =   375
      End
      Begin VB.TextBox txtCounter 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   3
         Left            =   240
         TabIndex        =   29
         Text            =   "0"
         Top             =   1920
         Width           =   855
      End
      Begin VB.CheckBox Chk_reload 
         Height          =   375
         Index           =   4
         Left            =   2400
         TabIndex        =   28
         Top             =   2325
         Width           =   375
      End
      Begin VB.CommandButton Cmd_clear 
         Caption         =   "C"
         Height          =   300
         Index           =   4
         Left            =   1800
         TabIndex        =   27
         Top             =   2295
         Width           =   375
      End
      Begin VB.CommandButton Cmd_read 
         Caption         =   "R"
         Height          =   300
         Index           =   4
         Left            =   1320
         TabIndex        =   26
         Top             =   2295
         Width           =   375
      End
      Begin VB.TextBox txtCounter 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   4
         Left            =   240
         TabIndex        =   25
         Text            =   "0"
         Top             =   2325
         Width           =   855
      End
      Begin VB.CheckBox Chk_reload 
         Height          =   375
         Index           =   5
         Left            =   2400
         TabIndex        =   24
         Top             =   2715
         Width           =   375
      End
      Begin VB.CommandButton Cmd_clear 
         Caption         =   "C"
         Height          =   300
         Index           =   5
         Left            =   1800
         TabIndex        =   23
         Top             =   2700
         Width           =   375
      End
      Begin VB.CommandButton Cmd_read 
         Caption         =   "R"
         Height          =   300
         Index           =   5
         Left            =   1320
         TabIndex        =   22
         Top             =   2700
         Width           =   375
      End
      Begin VB.TextBox txtCounter 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   5
         Left            =   240
         TabIndex        =   21
         Text            =   "0"
         Top             =   2715
         Width           =   855
      End
      Begin VB.CheckBox Chk_reload 
         Height          =   375
         Index           =   6
         Left            =   2400
         TabIndex        =   20
         Top             =   3120
         Width           =   375
      End
      Begin VB.CommandButton Cmd_clear 
         Caption         =   "C"
         Height          =   300
         Index           =   6
         Left            =   1800
         TabIndex        =   19
         Top             =   3105
         Width           =   375
      End
      Begin VB.CommandButton Cmd_read 
         Caption         =   "R"
         Height          =   300
         Index           =   6
         Left            =   1320
         TabIndex        =   18
         Top             =   3105
         Width           =   375
      End
      Begin VB.TextBox txtCounter 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   6
         Left            =   240
         TabIndex        =   17
         Text            =   "0"
         Top             =   3120
         Width           =   855
      End
      Begin VB.CheckBox Chk_reload 
         Height          =   375
         Index           =   7
         Left            =   2400
         TabIndex        =   16
         Top             =   3525
         Width           =   375
      End
      Begin VB.CommandButton Cmd_clear 
         Caption         =   "C"
         Height          =   300
         Index           =   7
         Left            =   1800
         TabIndex        =   15
         Top             =   3495
         Width           =   375
      End
      Begin VB.CommandButton Cmd_read 
         Caption         =   "R"
         Height          =   300
         Index           =   7
         Left            =   1320
         TabIndex        =   14
         Top             =   3495
         Width           =   375
      End
      Begin VB.TextBox txtCounter 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   7
         Left            =   240
         TabIndex        =   13
         Text            =   "0"
         Top             =   3525
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Auto reload"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2280
         TabIndex        =   48
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Value"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   47
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Read"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1200
         TabIndex        =   46
         Top             =   240
         Width           =   495
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         Caption         =   "Clear"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1800
         TabIndex        =   45
         Top             =   240
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4695
      Left            =   360
      TabIndex        =   1
      Top             =   240
      Width           =   1335
      Begin VB.CommandButton Cmd_apply 
         Caption         =   "Apply"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   11
         Top             =   3960
         Width           =   855
      End
      Begin VB.CheckBox Inport 
         Caption         =   "IN 07"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   7
         Left            =   240
         TabIndex        =   9
         Top             =   3465
         Width           =   1000
      End
      Begin VB.CheckBox Inport 
         Caption         =   "IN 06"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   6
         Left            =   240
         TabIndex        =   8
         Top             =   3060
         Width           =   1000
      End
      Begin VB.CheckBox Inport 
         Caption         =   "IN 05"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   5
         Left            =   240
         TabIndex        =   7
         Top             =   2655
         Width           =   1000
      End
      Begin VB.CheckBox Inport 
         Caption         =   "IN 04"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   4
         Left            =   240
         TabIndex        =   6
         Top             =   2265
         Width           =   1000
      End
      Begin VB.CheckBox Inport 
         Caption         =   "IN 03"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   3
         Left            =   240
         TabIndex        =   5
         Top             =   1860
         Width           =   1000
      End
      Begin VB.CheckBox Inport 
         Caption         =   "IN 02"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   2
         Left            =   240
         TabIndex        =   4
         Top             =   1455
         Width           =   1000
      End
      Begin VB.CheckBox Inport 
         Caption         =   "IN 01"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   1
         Left            =   240
         TabIndex        =   3
         Top             =   1065
         Width           =   1000
      End
      Begin VB.CheckBox Inport 
         Caption         =   "IN 00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   350
         Index           =   0
         Left            =   240
         TabIndex        =   2
         Top             =   660
         Width           =   1000
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Port"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   200
      Left            =   2040
      Top             =   4560
   End
   Begin VB.CommandButton Cmd_cancel 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2880
      TabIndex        =   0
      Top             =   4440
      Width           =   975
   End
End
Attribute VB_Name = "frmCounter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check1_Click()
    
End Sub

Private Sub chk_auto_reload_Click()
    If chk_auto_reload.Value = 1 Then
        Timer1.Enabled = True
    Else
        Timer1.Enabled = False
    End If
End Sub

Private Sub Chk_reload_Click(Index As Integer)
    Dim i As Integer
    Timer1.Enabled = False
    For i = 0 To POINT_MAX
        If Chk_reload(i).Value = 1 Then
            Timer1.Enabled = True
        End If
    Next i
    
End Sub

Private Sub Cmd_apply_Click()
    Dim channel As Integer
    Dim i As Integer
    Dim state As Integer
        
    channel = 0
    For i = 0 To 7
        If Inport(i).Value = 1 Then
            channel = channel + 2 ^ i
        End If
    Next i
    state = EMD820x_counter_mask_set(CardID, 0, channel)
    If state = NO_ERROR Then
        Cmd_apply.Enabled = False
    Else
        MsgBox "Error code:" + Str(state)
    End If
End Sub

Private Sub Cmd_cancel_Click()
    Unload Me
End Sub

Private Sub Cmd_clear_Click(Index As Integer)
'    Dim state As Long
    state = EMD820x_counter_clear(CardID, 0, Index)
End Sub

Private Sub Cmd_read_Click(Index As Integer)
    Dim state As Long
    Dim counter(0 To 7) As Long
    state = EMD820x_counter_read(CardID, 0, counter(0))
    txtCounter(Index).Text = Trim(Str(counter(Index)))
End Sub

Private Sub Form_Load()
    Dim i As Integer
    If CardType = CARD_TYPE_4IO Then
        For i = 4 To 7
            Inport(i).Visible = False
            txtCounter(i).Visible = False
            Cmd_read(i).Visible = False
            Cmd_clear(i).Visible = False
            Chk_reload(i).Visible = False
        Next i
        
        Cmd_apply.Top = Cmd_apply.Top - 1700
        Frame1.Height = Frame1.Height - 1800
        Frame3.Height = 2500
        Cmd_cancel.Top = Cmd_cancel.Top - 1600
        frmCounter.Height = frmCounter.Height - 1800
    End If
    state = EMD820x_counter_enable(CardID)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not Reboot_flag Then 'to prevent do the command after machine reboot,add reboot_flag to sign
        EMD820x_counter_disable (CardID)
    End If
    'Unload Me
End Sub

Private Sub Initial_help_Click()
    With frmMain.CommonDialog1
      .HelpFile = "sw820x.hlp"
      .HelpContext = 270
      .HelpCommand = cdlHelpContext
      .ShowHelp
    End With
End Sub

Private Sub Inport_Click(Index As Integer)
    Cmd_apply.Enabled = True
End Sub

Private Sub Timer1_Timer()
    Dim i As Integer
    Dim state As Long
    Dim counter(0 To 7) As Long
    
    state = EMD820x_counter_read(CardID, 0, counter(0))
    m_Global.Check_ERROR (state)
    
    For i = 0 To 7
        If Chk_reload(i).Value = 1 Then
            txtCounter(i).Text = Trim(Str(counter(i)))
        End If
    Next i
    
End Sub
