VERSION 5.00
Begin VB.Form frmSetPW 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Set Password"
   ClientHeight    =   3600
   ClientLeft      =   8115
   ClientTop       =   5250
   ClientWidth     =   4710
   Icon            =   "frmSetPW.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3600
   ScaleWidth      =   4710
   Begin VB.CommandButton Initial_help 
      Height          =   555
      Left            =   3480
      Picture         =   "frmSetPW.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2760
      Width           =   675
   End
   Begin VB.TextBox txtconform 
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   2640
      MaxLength       =   8
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   1920
      Width           =   1455
   End
   Begin VB.CommandButton CmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2160
      TabIndex        =   4
      Top             =   2760
      Width           =   1095
   End
   Begin VB.CommandButton CmdOk 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   600
      TabIndex        =   3
      Top             =   2760
      Width           =   1095
   End
   Begin VB.TextBox txtNewPassword 
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   2640
      MaxLength       =   8
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   1200
      Width           =   1455
   End
   Begin VB.TextBox txtOldPassword 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   2640
      MaxLength       =   8
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   480
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Conform password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   8
      Top             =   2040
      Width           =   1935
   End
   Begin VB.Label Label1 
      Caption         =   "NEW password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   7
      Top             =   1320
      Width           =   1935
   End
   Begin VB.Label Label4 
      Caption         =   "OLD password"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   6
      Top             =   600
      Width           =   1935
   End
End
Attribute VB_Name = "frmSetPW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Status As Integer

Private Sub CmdCancel_Click()
    Unload Me
End Sub

Private Sub CmdDefault_Click()
    Dim temp As Integer
    temp = MsgBox("It will set the password to default", vbYesNo + vbExclamation)
    If temp = vbYes Then
        Status = EMD820x_password_set_default(CardID)
        If Status = 0 Then
            Unload Me
            Unload frmMain
            frmCountDown.Show 1
        Else
            MsgBox "Change password Error:" + Str(Status), vbOKOnly, "ERROR"
        End If
    End If
End Sub

Private Sub CmdOk_Click()
    Dim NewPW(7) As Byte
    Dim OldPW(7) As Byte
    Dim temp_data As Byte
    Dim i As Integer
    Status = MsgBox("When success change the password,it will reboot EMD820x", vbOKCancel, "warning")
    If Status = vbOK Then
        If txtNewPassword.Text = txtconform.Text Then
            For i = 1 To Len(txtOldPassword.Text)
                'get Old password to oldPW
                temp_data = Asc(Mid(txtOldPassword, i, 1))
                'If temp_data <> "" Then
                    OldPW(i - 1) = temp_data
                'End If
            Next i
            For i = 1 To Len(txtNewPassword.Text)
                'get New password to NewPW
                temp_data = Asc(Mid(txtNewPassword, i, 1))
                'If temp_data <> "" Then
                    NewPW(i - 1) = Asc(Mid(txtNewPassword, i, 1))
                'End If
            Next i
        
            Status = EMD820x_password_change(CardID, OldPW(0), NewPW(0))
            If Status = 0 Then
                Reboot_flag = True
                Unload Me
                Unload frmMain
                frmCountDown.Show 1
            Else
                MsgBox "Password error:" + Str(Status), , "ERROR"
            End If
        Else
            MsgBox "Comform password error #2", , "ERROR"
        End If
    End If
End Sub

Private Sub Initial_help_Click()

   With frmMain.CommonDialog1
      .HelpFile = "sw820x.hlp"
      .HelpContext = 370
      .HelpCommand = cdlHelpContext
      .ShowHelp
    End With
End Sub

Private Sub txtconform_KeyPress(KeyAscii As Integer)
    If KeyAscii = &HD Then
        CmdOk_Click
    End If
    If Not (Chr$(KeyAscii) Like "[0-9]" Or KeyAscii < 31) Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtNewPassword_KeyPress(KeyAscii As Integer)

    If KeyAscii = &HD Then
        CmdOk_Click
    End If
    If Not (Chr$(KeyAscii) Like "[0-9]" Or KeyAscii < 31) Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtOldPassword_KeyPress(KeyAscii As Integer)

    If KeyAscii = &HD Then
        CmdOk_Click
    End If
    If Not (Chr$(KeyAscii) Like "[0-9]" Or KeyAscii < 31) Then
        KeyAscii = 0
    End If
End Sub
