VERSION 5.00
Begin VB.Form frmUnlock 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Unlock"
   ClientHeight    =   2190
   ClientLeft      =   7560
   ClientTop       =   4980
   ClientWidth     =   3375
   Icon            =   "frmUnlock.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2190
   ScaleWidth      =   3375
   Begin VB.CommandButton Initial_help 
      Height          =   555
      Left            =   120
      Picture         =   "frmUnlock.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1440
      Width           =   675
   End
   Begin VB.CommandButton CmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2160
      TabIndex        =   3
      Top             =   1440
      Width           =   975
   End
   Begin VB.CommandButton CmdOk 
      Caption         =   "Ok"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   960
      TabIndex        =   2
      Top             =   1440
      Width           =   975
   End
   Begin VB.TextBox txtPassword 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   1800
      MaxLength       =   8
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Password:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   1095
   End
End
Attribute VB_Name = "frmUnlock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdCancel_Click()
    Unload Me
End Sub

Private Sub CmdOk_Click()
    Dim PW(7) As Byte
    Dim i As Integer
    Dim port As Integer
    Dim time_value As Integer

    For i = 1 To Len(txtPassword)
        PW(i - 1) = Asc(Mid(txtPassword.Text, i, 1))
    Next i
    
    mintStatus = EMD820x_security_unlock(CardID, PW(0))
    If mintStatus = 0 Then
        EMD820x_port_read CardID, OUTPORT, port
        'EMD820x_port_set CardID, 0, 0
        
        frmMain.Timer1.Enabled = True
        frmMain.Auto.Enabled = True
        frmMain.Fr9201.Enabled = True
        frmMain.m_lock.Visible = False
        EMD820x_counter_disable CardID
        EMD820x_counter_mask_set CardID, 0, 0

        For i = 0 To 7
            EMD820x_counter_clear CardID, 0, i
            If (port And (2 ^ i)) > 0 Then
               frmMain.OutP(i).Value = 1
            Else
               frmMain.OutP(i).Value = 0
            End If
        Next i
        
        
        EMD820x_WDT_read CardID, time_value, state, enable_flag
        
        If enable_flag = 1 Then
            frmMain.Label_wdt.Caption = "WDT ENABLE"
        Else
            frmMain.Label_wdt.Caption = "WDT DISABLE"
        End If
        Unload Me
    Else
        MsgBox "Unlock error:" + Str(mintStatus), , "ERROR"
    End If
    
    
End Sub

Private Sub Initial_help_Click()

   With frmMain.CommonDialog1
      .HelpFile = "sw820x.hlp"
      .HelpContext = 380
      .HelpCommand = cdlHelpContext
      .ShowHelp
    End With
End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)
    If KeyAscii = &HD Then
        CmdOk_Click
    End If
End Sub
