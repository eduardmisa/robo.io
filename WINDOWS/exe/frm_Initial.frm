VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frm_Initial 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "EMD820x initial"
   ClientHeight    =   3885
   ClientLeft      =   7020
   ClientTop       =   4845
   ClientWidth     =   5610
   Icon            =   "frm_Initial.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3885
   ScaleWidth      =   5610
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3240
      Top             =   3120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Initial_help 
      Height          =   555
      Left            =   4080
      Picture         =   "frm_Initial.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   2880
      Width           =   675
   End
   Begin VB.TextBox txt_TimeOut_ms 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1028
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      MaxLength       =   5
      TabIndex        =   13
      Text            =   "1000"
      Top             =   3000
      Width           =   735
   End
   Begin VB.ComboBox ID 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      ItemData        =   "frm_Initial.frx":110C
      Left            =   1920
      List            =   "frm_Initial.frx":110E
      MousePointer    =   99  'Custom
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   240
      Width           =   735
   End
   Begin VB.TextBox txtHostPort 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      MaxLength       =   5
      TabIndex        =   4
      Text            =   "0"
      Top             =   1560
      Width           =   735
   End
   Begin VB.TextBox txtRemotePort 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1028
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      MaxLength       =   5
      TabIndex        =   5
      Text            =   "6936"
      Top             =   2400
      Width           =   735
   End
   Begin VB.TextBox txtIP 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1028
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   1920
      MaxLength       =   3
      TabIndex        =   0
      Text            =   "192"
      Top             =   960
      Width           =   735
   End
   Begin VB.TextBox txtIP 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1028
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   2760
      MaxLength       =   3
      TabIndex        =   1
      Text            =   "168"
      Top             =   960
      Width           =   735
   End
   Begin VB.TextBox txtIP 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1028
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   3600
      MaxLength       =   3
      TabIndex        =   2
      Text            =   "0"
      Top             =   960
      Width           =   735
   End
   Begin VB.TextBox txtIP 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1028
         SubFormatType   =   1
      EndProperty
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   4440
      MaxLength       =   3
      TabIndex        =   3
      Text            =   "100"
      Top             =   960
      Width           =   735
   End
   Begin VB.CommandButton cmdInitial 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3720
      TabIndex        =   6
      Top             =   2160
      Width           =   1215
   End
   Begin VB.Label Label5 
      Caption         =   "(When fill 0,it will choose by system)"
      Height          =   255
      Left            =   960
      TabIndex        =   15
      Top             =   2040
      Width           =   2655
   End
   Begin VB.Label Label4 
      Caption         =   "Time Out(ms):"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   12
      Top             =   3120
      Width           =   1335
   End
   Begin VB.Label label 
      Caption         =   "ID "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   840
      TabIndex        =   11
      Top             =   300
      Width           =   495
   End
   Begin VB.Label Label3 
      Caption         =   "Host port"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   480
      TabIndex        =   10
      Top             =   1680
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Remote port"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   2400
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "IP"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   840
      TabIndex        =   8
      Top             =   960
      Width           =   495
   End
End
Attribute VB_Name = "frm_Initial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim mintStatus As Long

Private Sub cmdInitial_Click()
    'Dim Cardid As Integer
    Dim i As Integer
    Dim ErrorFlag As Integer
    Dim ErrorCode As Integer
    ErrorFlag = 0

    If Val(txtHostPort.Text) > port_max Or Val(txtRemotePort.Text) > port_max Then
        ErrorFlag = 1
    Else
        CardID = Val(ID.Text)
    
        For i = 0 To 3
            If Val(txtIP(i).Text) <= IP_MAX Then
                IpAddress(i) = Val(txtIP(i).Text)
            Else
                MsgBox "IP ERROR  :" + Str(ErrorCode), , "ERROR"
            End If
        Next i
    
        'set the basic connect parameters
        Remote_port = Val(txtRemotePort.Text)
        mintStatus = EMD820x_initial(CardID, IpAddress(0), Val(txtHostPort.Text), Val(txtRemotePort.Text), Val(txt_TimeOut_ms.Text), CardType)

        'when success connect,call Main frame
        If mintStatus = 0 And ErrorFlag = 0 Then
            CardID = Val(ID.Text)
            Call frmMain.Show
            Unload Me
            frmUnlock.Show 1
        Else
            If ErrorFlag = 0 Then
                ErrorFlag = 3
                ErrorCode = mintStatus
            End If
        End If
    End If
    Select Case ErrorFlag
        Case 1
            MsgBox "Port ERROR  :" + Str(ErrorCode), , "ERROR"
        Case 2
            MsgBox "IP ERROR  :" + Str(ErrorCode), , "ERROR"
        Case 3
            MsgBox "Initial ERROR  :" + Str(ErrorCode), , "ERROR"
    End Select
End Sub

Private Sub Form_Load()
    Dim i As Integer
    If IpAddress(0) > 0 Then
        txtIP(0).Text = IpAddress(0)
        txtIP(1).Text = IpAddress(1)
        txtIP(2).Text = IpAddress(2)
        txtIP(3).Text = IpAddress(3)
    End If
    If Remote_port > 0 Then
        txtRemotePort.Text = Trim(Str(Remote_port))
    End If
    For i = 0 To 255
        ID.AddItem i
        Next i
    ID.ListIndex = 0
    'test dll is work correctly
    'mintStatus = EMD820x_initial(Val(txtHostPort.Text), Val(txtRemotePort.Text))
    Reboot_flag = False
End Sub

Private Sub Initial_help_Click()

   With CommonDialog1
      .HelpFile = "sw820x.hlp"
      .HelpContext = 140
      .HelpCommand = cdlHelpContext
      .ShowHelp
    End With
End Sub

'set the TextBox only input 0-9
Private Sub txtIP_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = &HD Then
        cmdInitial_Click
    End If
    If Not (Chr$(KeyAscii) Like "[0-9]" Or KeyAscii < 31) Then
        KeyAscii = 0
    End If
End Sub
