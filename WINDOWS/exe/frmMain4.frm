VERSION 5.00
Begin VB.Form frmMain4 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "EMD820x"
   ClientHeight    =   3420
   ClientLeft      =   8520
   ClientTop       =   4590
   ClientWidth     =   3015
   BeginProperty Font 
      Name            =   "MS SystemEx"
      Size            =   15
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4420
   ScaleMode       =   0  'User
   ScaleWidth      =   3015
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   0
      Top             =   2760
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   480
      Top             =   2760
   End
   Begin VB.CommandButton exit 
      Caption         =   "&Quit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      TabIndex        =   10
      Top             =   2760
      Width           =   645
   End
   Begin VB.CheckBox Auto 
      Caption         =   "&AUTO"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1200
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   2760
      Width           =   765
   End
   Begin VB.Frame Fr9201 
      Caption         =   "I/O"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   2595
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 03"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   3
         Left            =   1350
         TabIndex        =   8
         Top             =   1230
         Width           =   975
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 02"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   2
         Left            =   1350
         TabIndex        =   7
         Top             =   990
         Width           =   975
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 01"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   1350
         TabIndex        =   6
         Top             =   750
         Width           =   975
      End
      Begin VB.CheckBox OutP 
         Caption         =   "OUT 00"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   0
         Left            =   1350
         TabIndex        =   5
         Top             =   510
         Width           =   975
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 03"
         DragMode        =   1  'Automatic
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   3
         Left            =   210
         TabIndex        =   4
         Top             =   1230
         Width           =   855
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 02"
         DragMode        =   1  'Automatic
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   2
         Left            =   210
         TabIndex        =   3
         Top             =   990
         Width           =   855
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 01"
         DragMode        =   1  'Automatic
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   255
         Index           =   1
         Left            =   210
         TabIndex        =   2
         Top             =   750
         Width           =   855
      End
      Begin VB.CheckBox InP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IN 00"
         DragMode        =   1  'Automatic
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Index           =   0
         Left            =   210
         TabIndex        =   1
         Top             =   510
         Width           =   855
      End
   End
   Begin VB.Label txtCardID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   240
      Width           =   2055
   End
   Begin VB.Image Lock_ico 
      Height          =   480
      Index           =   0
      Left            =   2400
      Picture         =   "frmMain4.frx":0000
      Top             =   120
      Width           =   480
   End
   Begin VB.Image Lock_ico 
      Height          =   480
      Index           =   1
      Left            =   2400
      Picture         =   "frmMain4.frx":030A
      Top             =   120
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Menu m_file 
      Caption         =   "&File"
      Begin VB.Menu m_exit 
         Caption         =   "&Exit"
      End
   End
   Begin VB.Menu m_parameter 
      Caption         =   "&Parameter"
   End
   Begin VB.Menu m_security 
      Caption         =   "&Security"
      Begin VB.Menu m_chpassword 
         Caption         =   "&Change Password"
      End
      Begin VB.Menu m_pddefault 
         Caption         =   "&Set Password to default"
      End
   End
   Begin VB.Menu m_help 
      Caption         =   "&help"
   End
End
Attribute VB_Name = "frmMain4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim AUTO820xData As Byte
'Dim IP_Address(3) As Byte
Dim Status As Integer
'Public CardID As Integer
'Public mcCardID As Integer

Private Sub Auto_Click()
    Timer2.Enabled = Auto.Value
End Sub

Private Sub CmdUnlock_Click()
    frmUnlock.Show 1
        'Dim PW(7) As Byte
        'Dim i As Integer
        'For i = 1 To Len(txtpassword)
        '    PW(i - 1) = Asc(Mid(txtpassword.Text, i, 1))
        'Next i
        'array
        'mintStatus = EMD820x_unlock_security(mcCardID, PW(0))
        'If mintStatus = 0 Then
        '    Call frmMain.Show
        '    Unload Me
        'Else
        '    MsgBox "PASSWORD ERROR", , "ERROR"
        'End If
End Sub

Private Sub exit_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    AUTO820xData = 0
    txtCardID.Caption = "CardID " + Str(CardID) + " = " + frm_Initial.txtIP(0).Text + "." + frm_Initial.txtIP(1).Text + "." + frm_Initial.txtIP(2).Text + "." + frm_Initial.txtIP(3).Text
End Sub

Private Sub Lock_ico_Click(Index As Integer)
    If Lock_ico(0).Visible = True Then
        frmUnlock.Show 1
    End If
End Sub

Private Sub m_chpassword_Click()
    Dim password As String
    'If InputBox("Please input password(26476936) ...") = "26476936" Then
        frmSetPW.Show 1
    'Else
        'MsgBox "Password error !!!"
    'End If
End Sub

Private Sub m_exit_Click()
    Unload Me
End Sub

Private Sub m_parameter_Click()
    If Lock_ico(0).Visible = True Then
        MsgBox "Please unlock first", , "Warning"
    ElseIf Lock_ico(1).Visible = True Then
        Call frmParameter.Show
    End If
End Sub

Private Sub m_pddefault_Click()
    Dim temp As Integer
    temp = MsgBox("It will set the password to default", vbYesNo + vbExclamation)
    If temp = vbYes Then
        Status = EMD820x_set_default_password(CardID)
        If Status = 0 Then
            Unload Me
            'Unload frmMain
            frmCountDown.Show 1
            'Call frm_Initial.Show
        Else
            MsgBox "Initial Error", vbOKOnly, "ERROR"
        End If
    End If
End Sub

Private Sub OutP_Click(Index As Integer)
    EMD820x_set_point CardID, Index, OutP(Index).Value
End Sub

Private Sub Timer1_Timer()
    readin
    Read_Security_Status
End Sub

Private Sub readin()
    Dim i As Integer
    Dim InState As Integer
    Dim temp_data As Integer
    Dim Status As Long
    Status = EMD820x_read_port(CardID, INPUT_PORT, temp_data)
    For i = 0 To POINT_MAX_4IO
        InState = temp_data Mod 2
        'Status = EMD820x_read_point(frm_Initial.mcCardID, INPUT_PORT, i, InState)
        If InState = 0 Then
            InP(i).Value = 1
        Else
            temp_data = temp_data - 1
            InP(i).Value = 0
        End If
        temp_data = temp_data / 2
    Next i
End Sub

Private Sub Read_Security_Status()
    Dim lock_status As Integer
    Status = EMD820x_read_security_status(CardID, lock_status)
    If lock_status = 1 Then
        Lock_ico(0).Visible = False
        Lock_ico(1).Visible = True
    End If
    'Lock_ico(0).Visible = False
    'Lock_ico(1).Visible = False
    'Lock_ico(2).Visible = False
    'Lock_ico(lock_status).Visible = True
    

End Sub

Private Sub Timer2_Timer()
    'AUTO820xData = AUTO820xData Mod 8
    If AUTO820xData > POINT_MAX_4IO Then
        AUTO820xData = 0
    End If
    Dim i As Integer
    For i = 0 To POINT_MAX_4IO
        OutP(i).Value = 0
        'OutP_Click (i)
    Next i
    EMD820x_set_port CardID, 0
    OutP(AUTO820xData).Value = 1
    OutP_Click (AUTO820xData)
    AUTO820xData = AUTO820xData + 1
    
End Sub
