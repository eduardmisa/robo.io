Attribute VB_Name = "EMD820x"
Global Const INPORT = 1
Global Const OUTPORT = 0
Global Const CARD_TYPE_4IO = 1
Global Const CARD_TYPE_8IO = 2
Global Const POINT_MAX_4IO = 3
Global Const POINT_MAX_8IO = 7
Global Const POINT_MAX_TTL = 7
Global Const port_max = 65535
Global Const IP_MAX = 255

Public IpAddress(0 To 3) As Byte
Public CardID As Long
Public CardType As Byte
Public POINT_MAX As Integer
Public Reboot_flag As Boolean   'to event the program shutdown,set a flag
Public Remote_port As Long


Type PWData
   password(0 To 7) As Byte
End Type
    
    Public Declare Function EMD820x_initial Lib "EMD820x.dll" (ByVal CardID As Long, ByRef IP_Address As Byte, ByVal HostPort As Long, ByVal RemotePort As Long, ByVal TimeOut_ms As Integer, ByRef CardType As Byte) As Long
    Public Declare Function EMD820x_close Lib "EMD820x.dll" (ByVal CardID As Long) As Long
    
    Public Declare Function EMD820x_port_read Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByRef data As Integer) As Long
    Public Declare Function EMD820x_port_set Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByVal data As Integer) As Long
    Public Declare Function EMD820x_port_polarity_read Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByRef data As Integer) As Long
    Public Declare Function EMD820x_port_polarity_set Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByVal data As Integer) As Long
    
    Public Declare Function EMD820x_point_read Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByVal point As Integer, ByRef state As Byte) As Long
    Public Declare Function EMD820x_point_set Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByVal point As Integer, ByVal state As Integer) As Long
    Public Declare Function EMD820x_point_polarity_read Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByVal port As Integer, ByRef data As Integer) As Long
    Public Declare Function EMD820x_point_polarity_set Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByVal port As Integer, ByVal data As Integer) As Long

    Public Declare Function EMD820x_counter_mask_set Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByVal channel As Integer) As Long
    Public Declare Function EMD820x_counter_enable Lib "EMD820x.dll" (ByVal CardID As Long) As Long
    Public Declare Function EMD820x_counter_disable Lib "EMD820x.dll" (ByVal CardID As Long) As Long
    Public Declare Function EMD820x_counter_read Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByRef counter As Long) As Long
    Public Declare Function EMD820x_counter_clear Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer, ByVal channel As Integer) As Long
    
    Public Declare Function EMD820x_socket_port_change Lib "EMD820x.dll" (ByVal CardID As Long, ByVal port As Integer) As Long
    Public Declare Function EMD820x_IP_change Lib "EMD820x.dll" (ByVal CardID As Long, ByRef IP As Byte) As Long
    Public Declare Function EMD820x_reboot Lib "EMD820x.dll" (ByVal CardID As Long) As Long
    
    Public Declare Function EMD820x_security_unlock Lib "EMD820x.dll" (ByVal CardID As Long, ByRef password As Byte) As Long
    Public Declare Function EMD820x_security_status_read Lib "EMD820x.dll" (ByVal CardID As Long, ByRef lock_status As Integer) As Long
    Public Declare Function EMD820x_password_change Lib "EMD820x.dll" (ByVal CardID As Long, ByRef Oldpassword As Byte, ByRef password As Byte) As Long
    Public Declare Function EMD820x_password_set_default Lib "EMD820x.dll" (ByVal CardID As Long) As Long
    Public Declare Function EMD820x_firmware_version_read Lib "EMD820x.dll" (ByVal CardID As Long, ByRef data As Byte) As Long

    Public Declare Function EMD820x_WDT_enable Lib "EMD820x.dll" (ByVal CardID As Long) As Long
    Public Declare Function EMD820x_WDT_disable Lib "EMD820x.dll" (ByVal CardID As Long) As Long
    Public Declare Function EMD820x_WDT_read Lib "EMD820x.dll" (ByVal CardID As Long, ByRef time As Integer, ByRef state As Byte, ByRef enable As Byte) As Long
    Public Declare Function EMD820x_WDT_set Lib "EMD820x.dll" (ByVal CardID As Long, ByVal time As Integer, ByVal state As Byte) As Long


