using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.InteropServices;
namespace CardProject
{
    class EMD820x
    {

		public const short CARD_ID_MAX = 1999;
		public const short Password_MAX = 8;
		public const short IP_MAX = 4;
		public const short TIME_OUT_MAX = 50000;
		public const short TIME_OUT_MIN = 1000;

		public const short CARD_TYPE_4IO = 0;
		public const short CARD_TYPE_8IO = 1;

		public const short INPORT = 0;
		public const short OUTPORT = 1;
		public const short PORT_MAX = 1;

		public const short POINT_MAX_4IO = 3;
		public const short POINT_MAX_8IO = 7;
		public const short POINT_MAX_TTL = 7;

		public const short NO_ERROR = 0;
		public const short INITIAL_SOCKET_ERROR = 1;
		public const short IP_ADDRESS_ERROR = 2;
		public const short UNLOCK_ERROR = 3;
		public const short LOCK_COUNTER_ERROR = 4;
		public const short SET_SECURITY_ERROR = 5;

		public const short DEVICE_RW_ERROR = 100;
		public const short NO_CARD = 101;
		public const short DUPLICATE_ID = 102;

		public const short ID_ERROR = 300;
		public const short PORT_ERROR = 301;
		public const short IN_POINT_ERROR = 302;
		public const short OUT_POINT_ERROR = 303;
		public const short PARAMETERS_ERROR = 305;
		public const short CHANGE_SOCKET_ERROR = 306;
		public const short UNLOCK_SECURITY_ERROR = 307;
		public const short PASSWORD_ERROR = 308;
		public const short REBOOT_ERROR = 309;
		public const short TIME_OUT_ERROR = 310;
		public const short CREAT_SOCKET_ERROR = 311;
		public const short CHANGEIP_ERROR = 312;
		public const short MASK_CHANNEL_ERROR = 313;
		public const short COUNTER_ENABLE_ERROR = 314;
		public const short COUNTER_DISABLE_ERROR = 315;
		public const short COUNTER_READ_ERROR = 316;
        public const short COUNTER_CLEAR_ERROR = 317;
            
        //--------------------------------------------------------------
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_initial(UInt32 CardID,ref byte[] IP_Address,UInt16 Host_Port,UInt16 Remote_port,UInt16 TimeOut_ms,ref byte CardType);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_close(UInt32 CardID);

		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_port_read(UInt32 CardID,byte port,ref byte data);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_port_set(UInt32 CardID,byte port,byte data);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_port_polarity_read(UInt32 CardID,byte port,ref byte data);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_port_polarity_set(UInt32 CardID,byte port,byte data);

		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_point_read(UInt32 CardID,byte port,byte point,ref byte state);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_point_set(UInt32 CardID,byte port,byte point,byte state);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_point_polarity_read(UInt32 CardID,byte port,byte point,ref byte state);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_point_polarity_set(UInt32 CardID,byte port,byte point,byte state);

		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_counter_mask_set(UInt32 CardID,byte port,byte channel);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_counter_enable(UInt32 CardID);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_counter_disable(UInt32 CardID);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_counter_read(UInt32 CardID,byte port,ref UInt32[] counter);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_counter_clear(UInt32 CardID,byte port,byte channel);

		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_socket_port_change(UInt32 CardID,UInt16 Remote_port);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_IP_change(UInt32 CardID,ref byte[] IP);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_reboot(UInt32 CardID);

		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_security_unlock(UInt32 CardID,ref byte[] password);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_security_status_read(UInt32 CardID,ref byte lock_status);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_password_change(UInt32 CardID,ref byte[] Oldpassword,ref byte[] Password);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_password_set_default(UInt32 CardID);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_firmware_version_read(UInt32 CardID,ref byte[] data);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_write_mac(UInt32 CardID,ref byte[] mac);
		
		

		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_WDT_enable(UInt32 CardID);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_WDT_disable(UInt32 CardID);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_WDT_set(UInt32 CardID,UInt16 time,byte state);
		[DllImport("EMD820x.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		public static extern UInt32 EMD820x_WDT_read(UInt32 CardID,ref UInt16 time,ref byte state,ref byte enable);

    }
}
