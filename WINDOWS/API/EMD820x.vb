Module EMD820x


	public const CARD_ID_MAX = 1999
	public const Password_MAX = 8
	public const IP_MAX = 4
	public const TIME_OUT_MAX = 50000
	public const TIME_OUT_MIN = 1000

	public const CARD_TYPE_4IO = 0
	public const CARD_TYPE_8IO = 1

	public const INPORT = 0
	public const OUTPORT = 1
	public const PORT_MAX = 1

	public const POINT_MAX_4IO = 3
	public const POINT_MAX_8IO = 7
	public const POINT_MAX_TTL = 7

	public const NO_ERROR = 0
	public const INITIAL_SOCKET_ERROR = 1
	public const IP_ADDRESS_ERROR = 2
	public const UNLOCK_ERROR = 3
	public const LOCK_COUNTER_ERROR = 4
	public const SET_SECURITY_ERROR = 5

	public const DEVICE_RW_ERROR = 100
	public const NO_CARD = 101
	public const DUPLICATE_ID = 102

	public const ID_ERROR = 300
	public const PORT_ERROR = 301
	public const IN_POINT_ERROR = 302
	public const OUT_POINT_ERROR = 303
	public const PARAMETERS_ERROR = 305
	public const CHANGE_SOCKET_ERROR = 306
	public const UNLOCK_SECURITY_ERROR = 307
	public const PASSWORD_ERROR = 308
	public const REBOOT_ERROR = 309
	public const TIME_OUT_ERROR = 310
	public const CREAT_SOCKET_ERROR = 311
	public const CHANGEIP_ERROR = 312
	public const MASK_CHANNEL_ERROR = 313
	public const COUNTER_ENABLE_ERROR = 314
	public const COUNTER_DISABLE_ERROR = 315
	public const COUNTER_READ_ERROR = 316
    Public Const COUNTER_CLEAR_ERROR = 317
	'
    Public Declare Function EMD820x_initial Lib "EMD820x.dll" (ByVal CardID As UInt32, ByRef IP_Address As Byte, ByVal Host_Port As UInt16, ByVal Remote_port As UInt16, ByVal TimeOut_ms As UInt16, ByRef CardType As Byte) As UInt32
    Public Declare Function EMD820x_close Lib "EMD820x.dll" (ByVal CardID As UInt32) As UInt32
    Public Declare Function EMD820x_port_read Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByRef data As Byte) As UInt32
    Public Declare Function EMD820x_port_set Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByVal data As Byte) As UInt32
    Public Declare Function EMD820x_port_polarity_read Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByRef data As Byte) As UInt32
    Public Declare Function EMD820x_port_polarity_set Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByVal data As Byte) As UInt32
    Public Declare Function EMD820x_point_read Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByVal point As Byte, ByRef state As Byte) As UInt32
    Public Declare Function EMD820x_point_set Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByVal point As Byte, ByVal state As Byte) As UInt32
    Public Declare Function EMD820x_point_polarity_read Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByVal point As Byte, ByRef state As Byte) As UInt32
    Public Declare Function EMD820x_point_polarity_set Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByVal point As Byte, ByVal state As Byte) As UInt32
    Public Declare Function EMD820x_counter_mask_set Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByVal channel As Byte) As UInt32
    Public Declare Function EMD820x_counter_enable Lib "EMD820x.dll" (ByVal CardID As UInt32) As UInt32
    Public Declare Function EMD820x_counter_disable Lib "EMD820x.dll" (ByVal CardID As UInt32) As UInt32
    Public Declare Function EMD820x_counter_read Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByRef counter As UInt32) As UInt32
    Public Declare Function EMD820x_counter_clear Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal port As Byte, ByVal channel As Byte) As UInt32
    Public Declare Function EMD820x_socket_port_change Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal Remote_port As UInt16) As UInt32
    Public Declare Function EMD820x_IP_change Lib "EMD820x.dll" (ByVal CardID As UInt32, ByRef IP As Byte) As UInt32
    Public Declare Function EMD820x_reboot Lib "EMD820x.dll" (ByVal CardID As UInt32) As UInt32
    Public Declare Function EMD820x_security_unlock Lib "EMD820x.dll" (ByVal CardID As UInt32, ByRef password As Byte) As UInt32
    Public Declare Function EMD820x_security_status_read Lib "EMD820x.dll" (ByVal CardID As UInt32, ByRef lock_status As Byte) As UInt32
    Public Declare Function EMD820x_password_change Lib "EMD820x.dll" (ByVal CardID As UInt32, ByRef Oldpassword As Byte, ByRef Password As Byte) As UInt32
    Public Declare Function EMD820x_password_set_default Lib "EMD820x.dll" (ByVal CardID As UInt32) As UInt32
    Public Declare Function EMD820x_firmware_version_read Lib "EMD820x.dll" (ByVal CardID As UInt32, ByRef data As Byte) As UInt32
    Public Declare Function EMD820x_write_mac Lib "EMD820x.dll" (ByVal CardID As UInt32, ByRef mac As Byte) As UInt32

	
    Public Declare Function EMD820x_WDT_enable Lib "EMD820x.dll" (ByVal CardID As UInt32) As UInt32
    Public Declare Function EMD820x_WDT_disable Lib "EMD820x.dll" (ByVal CardID As UInt32) As UInt32
    Public Declare Function EMD820x_WDT_read Lib "EMD820x.dll" (ByVal CardID As UInt32, ByRef time As UInt16, ByRef state As Byte, ByRef enable As Byte) As UInt32
    Public Declare Function EMD820x_WDT_set Lib "EMD820x.dll" (ByVal CardID As UInt32, ByVal time As UInt16, ByVal state As Byte) As UInt32

End Module