#include <stdio.h>
#include "EMD820x.dll"

int OB_EMD820x_initial () {
    
    u32 CardID = 0;
    
    u8 IP_address[4];
    IP_address[0] = 192;
    IP_address[1] = 168;
    IP_address[2] = 0;
    IP_address[3] = 100;
    
    u16 host_port = 15120;
    
    u16 remote_port = 6936;
    
    u16 timeout_ms = 1000;
    
    u8 module_type = 8;
    
    u32 status = EMD820x_initial(CardID, IP_address, host_port, remote_port, timeout_ms, &module_type);

    printf("%lu \n", status);
    
    printf("I'm in init");
    
    return 0;
}

int main(int argc, const char **argv) {

    printf("******** IO ETHERNET ********\n");

    return OB_EMD820x_initial();
}
