import sys
from enum import Enum
# import only system from os
from os import system, name
# import sleep to show output for some time period
from time import sleep
# mobus import
from pymodbus.client.sync import ModbusTcpClient


client = ModbusTcpClient('192.168.0.100')
client.connect()


class FIELDSETS(Enum):
	F1 = [1,0,0,0,0,0,0,0] # NORMAL
	F2 = [1,0,0,0,1,0,0,0] # BYPASS
	F3 = [1,0,0,0,0,1,0,0] # 
	F4 = [1,0,0,0,1,1,0,0] #
	F5 = [1,0,0,0,0,0,1,0] #
	F6 = [1,0,0,0,1,0,1,0] #
	F7 = [1,0,0,0,0,1,1,0] #
	F8 = [1,0,0,0,1,1,1,0] #


def command(arg):

	val = FIELDSETS[arg].value

	client.write_coils(0, val)

def main():

	arg = sys.argv[1]

	print(sys.argv[1])

	command(str(arg))

	result = client.read_coils(0, 8)

	print(result.bits)
	
	client.close()

main()